# Cardinal FaaS Framework

Sample project `common/makefile.mk`:

```
ROOT_DIR := $(PWD)
COMMON := ${ROOT_DIR}/common
CODE_DIR := ${ROOT_DIR}/code
TOOLS := aws vault docker cw
TOOLS_NPM :=
FRAMEWORK_DIR := ${COMMON}/framework
FRAMEWORK_REPO := git@code.stanford.edu:et-iedo-public/cardinal-faas-framework.git

## BUILD DIR
ifndef BUILD_DIR
        BUILD_DIR := ${ROOT_DIR}/build
endif

## FRAMEWORK INSTALL
ifeq ($(MAKELEVEL),0)
	ifeq ($(wildcard ${FRAMEWORK_DIR}/.git/),)
		_ := $(shell >&2 echo)
		_ := $(shell echo Installing framework from GIT into ${FRAMEWORK_DIR}...)
		_ := $(shell mkdir -p ${FRAMEWORK_DIR})
		_ := $(shell git clone ${FRAMEWORK_REPO} ${FRAMEWORK_DIR})
	else
		_ := $(shell cd ${FRAMEWORK_DIR} && git pull
	endif
endif

include ${FRAMEWORK_DIR}/makefile_parts/framework.mk

# APP SPECIFIC INCLUDES & MAKEFILE TARGETS HERE
include ${FRAMEWORK_DIR}/makefile_parts/resources.mk
# ...
```
