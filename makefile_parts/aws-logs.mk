##############
## AWS LOGS ##
##############

.PHONY: logs
logs: #### Show logs [ OPTIONS: logs <log_group_name> ]
	@echo ${APP_NAME}-${STAGE} ; \
	if [[ "$(filter-out $@, $(MAKECMDGOALS))" == "" ]] ; then \
		groups="$(shell cw ls groups --region=${AWS_REGION} | awk "/\/aws\/[a-zA-Z0-9_-]+\/([a-z]+-[a-z]+-[0-9]+\.)?${APP_NAME}-${STAGE}/")" ; \
		args="-t -n" ; \
	else \
		groups="$(filter-out $@, $(MAKECMDGOALS))" ; \
		args="-t" ; \
	fi ; \
	echo "Tailing logs [ $${groups} ] ..." ; \
	cw tail $${args} -b 1m -f $${groups}

# EOF
