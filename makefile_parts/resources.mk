###############
## RESOURCES ##
###############

export RESOURCES_TYPE := resources

.PHONY: resources-init
resources-init: export RESOURCES_DIR=${BUILD_DIR}/${RESOURCES_TYPE}
resources-init: ### Resources initialize code
	@${SCRIPTS_DIR}/terraform-init.sh -d ${RESOURCES_DIR} -c ${RESOURCES_TYPE}
	@cd ${RESOURCES_DIR} ; \
	if [[ ! -d .terraform ]] ; then ${TERRAFORM} init -no-color -backend-config=./backend.tfvars ; fi

.PHONY: resources-export
resources-export: export RESOURCES_DIR=${BUILD_DIR}/${RESOURCES_TYPE}
resources-export: ### Resources export configuration
	@cd ${RESOURCES_DIR} ; \
	${TERRAFORM} output -no-color -json | jq 'with_entries(.value |= .value)' > ${RESOURCES_DIR}/resources.json

.PHONY: resources-deploy
resources-deploy: export RESOURCES_DIR=${BUILD_DIR}/${RESOURCES_TYPE}
resources-deploy: ### Resources deployment
	@cd ${RESOURCES_DIR} ; ${TERRAFORM} apply -no-color -auto-approve

.PHONY: resources-remove
resources-remove: export RESOURCES_DIR=${BUILD_DIR}/${RESOURCES_TYPE}
resources-remove: ### Resources removal
	@cd ${RESOURCES_DIR} ; ${TERRAFORM} destroy -no-color -auto-approve

.PHONY: resources-info
resources-info: export RESOURCES_DIR=${BUILD_DIR}/${RESOURCES_TYPE}
resources-info: ### Resources information
	@cd ${RESOURCES_DIR} ; ${TERRAFORM} output -no-color -json | jq 'with_entries(.value |= .value) | del(.var)' || true

# EOF
