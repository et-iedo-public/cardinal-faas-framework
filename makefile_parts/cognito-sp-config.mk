##############################
## COGNITO SP CONFIGURATION ##
##############################

export RESOURCES_TYPE := resources

.PHONY: cognito-sp-config
cognito-sp-config: export RESOURCES_DIR=${BUILD_DIR}/${RESOURCES_TYPE}
cognito-sp-config: #### Generate Cognito SP Configuration
	@${SCRIPTS_DIR}/cognito-sp-config.sh

# EOF
