#############
## BACKEND ##
#############

export BACKEND_TYPE := backend

.PHONY: backend-init
backend-init: export BACKEND_DIR=${BUILD_DIR}/${BACKEND_TYPE}
backend-init: ### Backend initialize code
	@cd ${BACKEND_DIR} ; if [[ ! -d node_modules ]] ; then \
	${NPM} i --no-color --loglevel=error --legacy-bundling 2>/dev/null | sed 's/\x1B\[[0-9;]\+[A-Za-z]//g' ; fi

.PHONY: backend-export
backend-export: export BACKEND_DIR=${BUILD_DIR}/${BACKEND_TYPE}
backend-export: ### Backend export configuration
	@cd ${BACKEND_DIR} && ./node_modules/.bin/sls --no-color manifest --silent 2>/dev/null >/dev/null || true

.PHONY: backend-deploy
backend-deploy: export BACKEND_DIR=${BUILD_DIR}/${BACKEND_TYPE}
backend-deploy: ### Backend deployment
	@cd ${BACKEND_DIR} && ./node_modules/.bin/sls --no-color deploy 2>/dev/null

.PHONY: backend-remove
backend-remove: export BACKEND_DIR=${BUILD_DIR}/${BACKEND_TYPE}
backend-remove: ### Backend removal
	@cd ${BACKEND_DIR} && ./node_modules/.bin/sls --no-color remove 2>/dev/null || true

.PHONY: backend-info
backend-info: export BACKEND_DIR=${BUILD_DIR}/${BACKEND_TYPE}
backend-info: ### Backend information
	@echo
	@cd ${BACKEND_DIR} && ./node_modules/.bin/sls info --verbose 2>/dev/null || true

# EOF
