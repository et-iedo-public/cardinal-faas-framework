###########################
# FRAMEWORK BASE MAKEFILE #
###########################

SHELL := /bin/bash -e -O extglob -O nullglob -o pipefail
SCRIPTS_DIR := ${FRAMEWORK_DIR}/scripts
DOTENV_SCHEMA := ${COMMON}/env.schema
DOTENV_DEFAULTS := ${COMMON}/env.defaults

## DEPS
ifndef TERRAFORM
	TERRAFORM := terraform
endif
ifndef NPM
	NPM := npm
endif
TOOLS_PACKAGES := ${TERRAFORM} ${NPM} jq ${TOOLS}
TOOLS_NPM_PACKAGES := dotenv-expand dotenv-extended ${TOOLS_NPM}

## INIT
ifneq ("$(wildcard ${BUILD_DIR}/.env)","")
	include ${BUILD_DIR}/.env
	.DEFAULT_GOAL := help
else
	.DEFAULT_GOAL := init
endif

export

## TARGETS

.PHONY: help
help: TITLES=common::$(shell echo '##') components::$(shell echo '###') application::$(shell echo '####')
help: ## Show this help page
	@printf '\n¯\_(ツ)_/¯ \e[1;35m%-6s\e[m ¯\_(ツ)_/¯\n' "MAKEFILE TARGETS FOR PROJECT: ${APP_NAME}"
	@line=$(shell printf -- '-%.0s' {1..79}) ; \
	for index in ${TITLES}; do \
		title="$${index%%::*}" ; sep="$${index##*::}" ; \
		files="$(filter %.mk, $(MAKEFILE_LIST))" ; \
		cat $${files} | egrep "^[a-zA-Z_-]+:.*? $${sep} .*$$" | sort | \
		awk 'BEGIN {FS = ":.*?# "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' | \
		sed -e $$'1i\\\n\\\n'"$${line}"$$'\\\n\033[32m'"$${title}"$$'\033[0m\\\n'"$${line}" || true ; \
	done
	@echo

.PHONY: check-deps
check-deps: ## Check if dependencies are installed
	@${SCRIPTS_DIR}/check-deps.sh

.PHONY: framework-install
framework-install: ## Perform GIT clean install of framework
	@echo Installing framework from GIT into ${FRAMEWORK_DIR}...
	@rm -rf ${FRAMEWORK_DIR}
	@mkdir -p ${FRAMEWORK_DIR}
	@git clone ${FRAMEWORK_REPO} ${FRAMEWORK_DIR}

.PHONY: framework-update
framework-update: ## Perform GIT update of framework
	@echo Updating framework from GIT into ${FRAMEWORK_DIR}...
	@cd ${FRAMEWORK_DIR} ; git pull

.PHONY: init
init: clean check-deps configure

.PHONY: configure
configure: ## Configure the application
	@${SCRIPTS_DIR}/configure.sh

.PHONY: build
build: ## Run the build script
	@${SCRIPTS_DIR}/build.sh

.PHONY: clean
clean: ## Remove the build directory
	@rm -rf ${BUILD_DIR}

.PHONY: show-env
show-env: ## Show all env var values
	@set -o posix ; set

# EOF
