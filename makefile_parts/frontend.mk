##############
## FRONTEND ##
##############

FRONTEND_DIR := ${BUILD_DIR}/frontend
export

.PHONY: frontend-init
frontend-init: ### Frontend initialize code
	@cat ${BACKEND_DIR}/.serverless/manifest.json | \
	jq -c '. | {ServiceEndpoint: .[].urls.base}' 2>/dev/null > ${FRONTEND_DIR}/js/config.json || \
	echo '{}' > ${FRONTEND_DIR}/js/config.json

.PHONY: frontend-deploy
frontend-deploy: build resources-init resources-export backend-init backend-export frontend-init ### Frontend deployment
	@cd ${BACKEND_DIR} && ./node_modules/.bin/sls --no-color client deploy --no-confirm

.PHONY: frontend-remove
frontend-remove: build resources-init resources-export backend-init backend-export frontend-init ### Frontend removal
	@cd ${BACKEND_DIR} && ./node_modules/.bin/sls --no-color client remove --no-confirm

# EOF
