#!/bin/bash -e

echo

####
# CHECK FOR TOOLS
####

TOOLS_ERROR=0
for t in ${TOOLS_PACKAGES} ; do
    if ! type ${t} > /dev/null 2>&1 ; then
        printf "\033[31m%s\033[0m\n\n" "ERROR: ${t} command not found. Please install pre-reqs."
        TOOLS_ERROR=1
    fi
done
if [[ ${TOOLS_ERROR} -eq 1 ]] ; then exit 1 ; fi

####
# CHECK FOR NPM PACKAGES
####

NPM_ERROR=0
for n in ${TOOLS_NPM_PACKAGES} ; do
    set +e
    npm -g ls --depth=0 ${n} >/dev/null
    NPM_RETVAL=$?
    set -e
    if [[ ${NPM_RETVAL} -eq 1 ]] ; then
        printf "\033[31m%s\033[0m\n\n" "ERROR: ${n} npm package not found. Please install pre-reqs."
        NPM_ERROR=1
    fi
done
if [[ ${NPM_ERROR} -eq 1 ]] ; then exit 1 ; fi

# Print OK message if all is good
printf "\033[32m%s\033[0m\n\n" "OK: All pre-reqs are installed."
