#!/bin/bash -e

# Sync or cp ${CODE_DIR} to ${BUILD_DIR}
if [[ -d ${CODE_DIR} ]] ; then
    if [[ -d ${BUILD_DIR} ]] ; then
        echo "Update ${BUILD_DIR} ..."
        rsync -dr ${CODE_DIR}/ ${BUILD_DIR}
    else
        echo "Copy ${BUILD_DIR} from ${CODE_DIR} ..."
        cp -r ${CODE_DIR} ${BUILD_DIR}
    fi
fi
