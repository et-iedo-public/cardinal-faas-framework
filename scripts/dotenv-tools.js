var dotenvExpand = require('dotenv-expand');
var dotenvExtended = require('dotenv-extended');
var fs = require('fs');
var path = require('path');

var BUILD_DIR = process.env['BUILD_DIR'] || 'build';
var DOTENV_DEFAULTS = process.env['DOTENV_DEFAULTS'] || 'common/env.defaults';

function validate() {
    var DOTENV =  BUILD_DIR + '/env.entry.temp';
    var DOTENV_SCHEMA = BUILD_DIR + '/env.schema.temp';
    try {
        dotenvExtended.load({
            encoding: 'utf8',
            silent: true,
            path: DOTENV,
            defaults: DOTENV_DEFAULTS,
            schema: DOTENV_SCHEMA,
            errorOnMissing: true,
            errorOnRegex: true,
            errorOnExtra: false,
            includeProcessEnv: false,
            assignToProcessEnv: false,
            overrideProcessEnv: false
        });
    } catch(error) {
        var contents = fs.readFileSync(DOTENV_SCHEMA, 'utf8');
        console.log('\n>> ERROR: ' + error.message);
        console.log('>> MUST CONFORM TO: ' + contents.trim() + '\n');
        process.exit(1);
    }
}

function generate() {
    var DOTENV = BUILD_DIR + '/env.temp';
    var DOTENV_SCHEMA = process.env['DOTENV_SCHEMA'] || 'common/env.schema';
    try {
        var myEnv = {
            parsed: dotenvExtended.load({
                encoding: 'utf8',
                silent: true,
                path: DOTENV,
                defaults: DOTENV_DEFAULTS,
                schema: DOTENV_SCHEMA,
                errorOnMissing: true,
                errorOnRegex: true,
                errorOnExtra: true,
                includeProcessEnv: false,
                assignToProcessEnv: false,
                overrideProcessEnv: false
            }),
        };
        dotenvExpand(myEnv);
        var output = '';
        for (let [key, value] of Object.entries(myEnv.parsed)) {
            output += `${key}=${value}\n`;
        };
        fs.writeFile(BUILD_DIR + '/.env', output, (err) => { if (err) throw err; });
        console.log('Generated combined dotenv.');
    } catch(error) {
        var contents = fs.readFileSync(DOTENV_SCHEMA, 'utf8');
        console.log('\n>> ERROR: ' + error.message);
        console.log('>> MUST CONFORM TO:\n' + contents.trim());
        console.log('>> ERROR: PLEASE CORRECT AND TRY AGAIN\n');
        process.exit(1);
    }
}

var myArgs = process.argv.slice(2);
switch (myArgs[0]) {
    case 'validate':
        validate();
        break;
    case 'generate':
        generate();
        break;
    default:
        console.log('Usage: ' + path.basename(__filename) + ' <validate|generate>');
        process.exit(1);
}
