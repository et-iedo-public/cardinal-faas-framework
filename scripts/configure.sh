#!/bin/bash

set -e

## BEGIN & VALIDATE DOTENV_DEFAULTS
if [[ ! -d ${BUILD_DIR} ]] ; then mkdir -p ${BUILD_DIR} ; fi
echo "------------------------------------------------------------"
echo "The following variables are already set in $(basename ${DOTENV_DEFAULTS}):"
echo "------------------------------------------------------------"
for default in $(cat ${DOTENV_DEFAULTS} | egrep -v "^(#|$)" | sed -e 's/=.*/=/') ; do
    cat ${DOTENV_DEFAULTS} | egrep "^${default}"
    eval export $(cat ${DOTENV_DEFAULTS} | egrep "^${default}" | sed -e "s/=/='/" -e "s/$/'/")
    cat ${DOTENV_SCHEMA} | egrep "^${default}" > ${BUILD_DIR}/env.schema.temp
    NODE_PATH="$(npm root -g)" node ${SCRIPTS_DIR}/dotenv-tools.js validate
    rm -f ${BUILD_DIR}/env.schema.temp
done
echo "------------------------------------------------------------"
echo

## INPUT VALUES DEFINED IN SCHEMA & VALIDATE
echo "Please enter the remaining information to setup deployment:"
echo
for schema in $(cat ${DOTENV_SCHEMA} | egrep -v "^(#|$)") ; do
    echo "${schema}" > ${BUILD_DIR}/env.schema.temp
    tag="$(echo ${schema} | sed -e 's/=.*//')"
    if [[ $(egrep "^${tag}=" ${DOTENV_DEFAULTS}) ]] ; then continue ; fi
    pass=0
    while [[ ${pass} -eq 0 ]] ; do
        IFS= read -r -p "Enter value for ${tag}: " input 2>&1
        eval ${tag}="\"${input}\""
        if [[ "${input}" != "" ]] ; then
            echo "${tag}=${input}" > ${BUILD_DIR}/env.entry.temp
            NODE_PATH="$(npm root -g)" node ${SCRIPTS_DIR}/dotenv-tools.js validate && pass=1 || pass=0
            rm -f ${BUILD_DIR}/env.entry.temp
        fi
    done
    echo "${tag}=${input}" >> ${BUILD_DIR}/env.temp
    rm -f ${BUILD_DIR}/env.schema.temp
done
echo

## GENERATE DOTENV
set +e
NODE_PATH="$(npm root -g)" node ${SCRIPTS_DIR}/dotenv-tools.js generate
rm -f ${BUILD_DIR}/env.schema.temp ${BUILD_DIR}/env.entry.temp ${BUILD_DIR}/env.temp

## EDGE CASES
# Creates APP_NAME_STAGE_ALPHANUM, a camel case var derived from APP_NAME and APP_STAGE,
# that substitutes out non-alphanumberic characters (for cognito, waf, etc)
echo "APP_NAME_STAGE_ALPHANUM=$(echo ${APP_NAME}-${STAGE} | perl -pe 's/(?:\b|\W+|_)(\p{Ll})/\u$1/g')" >> ${BUILD_DIR}/.env
