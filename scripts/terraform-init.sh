#!/bin/bash -e

DIRECTORY=""
CATEGORY=""

usage() {
    echo "usage: $0 [[[-d directory ] [-c category]] | [-h]]"
}

while [[ ${DIRECTORY} == "" ]] || [[ ${CATEGORY} == "" ]] ; do
    case $1 in
        -d | --directory )
            if [[ ! -z $2 ]] ; then
                DIRECTORY=$2
                shift
                shift
            else
                usage
                exit 1
            fi
            ;;
        -c | --category )
            if [[ ! -z $2 ]] ; then
                CATEGORY=$2
                shift
                shift
            else
                usage
                exit 1
            fi
            ;;
        -h | --help )
            usage
            exit
            ;;
        * )
            usage
            exit 1
    esac
done

# Generate Terraform vars.tf and *.tfvars from dotenv
if [[ -d ${DIRECTORY} ]]; then
    # Get AWS account number
    AWS_ACCOUNT="$(aws sts get-caller-identity --query Account --output text || false)"
    # Make vars.tf
    rm -f ${DIRECTORY}/vars.tf
    cat ${BUILD_DIR}/.env | egrep -v "(^#|^$)" | sed -e 's/= */=/' -e 's/\\#/#/g' | egrep -v '=({|\[)' | \
        sed 's/\([a-zA-Z0-9_]*\)=.*/variable "\1" { type = string }/' >> ${DIRECTORY}/vars.tf
    cat ${BUILD_DIR}/.env | egrep -v "(^#|^$)" | sed -e 's/= */=/' -e 's/\\#/#/g' | egrep '={' | \
        sed 's/\([a-zA-Z0-9_]*\)=.*/variable "\1" { type = map }/' >> ${DIRECTORY}/vars.tf
    cat ${BUILD_DIR}/.env | egrep -v "(^#|^$)" | sed -e 's/= */=/' -e 's/\\#/#/g' | egrep '=\[' | \
        sed 's/\([a-zA-Z0-9_]*\)=.*/variable "\1" { type = list }/' >> ${DIRECTORY}/vars.tf
    # Add outputs to vars.tf
    echo -e "\noutput \"var\" {\n  value = {" >> ${DIRECTORY}/vars.tf
    cat ${BUILD_DIR}/.env | egrep -v "(^#|^$)" | sed 's/= */=/' | \
        sed 's/\([a-zA-Z0-9_]*\)=.*/    "\1" = var.\1/' >> ${DIRECTORY}/vars.tf
    echo -e "  }\n}" >> ${DIRECTORY}/vars.tf
    # Make terraform.tfvars
    rm -f ${DIRECTORY}/terraform.tfvars
    cat ${BUILD_DIR}/.env | egrep -v "(^#|^$)" | sed -e 's/= */=/' -e 's/\\#/#/g' | egrep -v '=("|{|\[)' | \
        sed 's/=\(.*\)/ = "\1"/' >> ${DIRECTORY}/terraform.tfvars
    cat ${BUILD_DIR}/.env | egrep -v "(^#|^$)" | sed -e 's/= */=/' -e 's/\\#/#/g' | egrep '=("|{|\[)' | \
        sed 's/=\(.*\)/ = \1/' >> ${DIRECTORY}/terraform.tfvars
    # Make backend.tfvars
    echo "bucket = \"${AWS_ACCOUNT}-${TAG_TEAM}-${STAGE}-terraform\"" > ${DIRECTORY}/backend.tfvars
    echo "key = \"services/${APP_NAME}-${STAGE}/${APP_NAME}-${STAGE}-${CATEGORY}.tfstate\"" >> ${DIRECTORY}/backend.tfvars
    echo "region = \"${AWS_REGION}\"" >> ${DIRECTORY}/backend.tfvars
fi
