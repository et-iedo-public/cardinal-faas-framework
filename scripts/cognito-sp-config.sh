#!/bin/bash

USER_POOL_ID=$(cat ${RESOURCES_DIR}/resources.json | jq -r '.cognito.user_pool_id')
COGNITO_FQDN=$(cat ${RESOURCES_DIR}/resources.json | jq -r '.cognito.fqdn')
METADATA_TEMPLATE=$(echo ${COGNITO_SP_METADATA_TEMPLATE} | sed -e 's/{{/{/g' -e 's/}}/}/g')

export COGNITO_SP_CERT=$(aws cognito-idp get-signing-certificate --user-pool-id ${USER_POOL_ID} --region ${AWS_REGION} | jq -r '.Certificate')
export COGNITO_SP_ENTITY_ID="urn:amazon:cognito:sp:${USER_POOL_ID}"
export COGNITO_SP_IDP_LOGIN="https://${COGNITO_FQDN}/oauth2/authorize"
export COGNITO_SP_IDP_RESP="https://${COGNITO_FQDN}/saml2/idpresponse"

printf "\n\e[0;33m\e[4mMETADATA\e[0m\n"
eval printf "${METADATA_TEMPLATE}" | xmllint --format - | sed 1d
printf "\n\e[0;33m\e[4mLOGIN URL\e[0m\n"
printf "${COGNITO_SP_IDP_LOGIN}\n"
printf "\n\e[0;33m\e[4mRESPONSE URL\e[0m\n"
printf "${COGNITO_SP_IDP_RESP}\n"
echo
